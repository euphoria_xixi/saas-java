<#assign className = table.className>
<#assign classNameLower = className?uncap_first> 
package ${basepackage}.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ${basepackage}.dao.${className}Dao;
import ${basepackage}.vo.${className}VO;
import com.yhy.common.service.BaseService;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-17 下午3:36 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class ${className}Service extends BaseService<${className}VO> {

	@Autowired
	private  ${className}Dao ${classNameLower}Dao;

	@Override
	protected ${className}Dao getDao() {
		return ${classNameLower}Dao;
	}
	
}

package com.yhy.job.action;

import com.yhy.common.action.BaseMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.job.dto.QuartzJobDTO;
import com.yhy.job.service.mng.QuartzJobMngService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-16 下午2:51 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/jobMng", produces="application/json;charset=UTF-8")
public class QuartzJobMngMngAction extends BaseMngAction<QuartzJobDTO> {


    @Override
    protected QuartzJobMngService getBaseService() {
        return SpringContextHolder.getBean(QuartzJobMngService.class);
    }


    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = QuartzJobDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody QuartzJobDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = QuartzJobDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody QuartzJobDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "QuartzJobDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody QuartzJobDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "QuartzJobDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody QuartzJobDTO baseDTO) {
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataType = "QuartzJobDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody QuartzJobDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/pauseJob/{id}",method = {RequestMethod.PUT})
    @ResponseBody
    @ApiOperation(value="暂停", notes="暂停操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "暂停", required = true, dataType = "QuartzJobDTO")
    })
    public AppReturnMsg doPause(HttpServletRequest request, HttpServletResponse response,@PathVariable String id) {
        getBaseService().doPause(id);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"");
    }

    @PreCheckPermission(roles = {"SYS_ADMIN"})
    @RequestMapping(value="/runJob/{id}",method = {RequestMethod.PUT})
    @ResponseBody
    @ApiOperation(value="执行任务", notes="执行任务操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "执行任务", required = true, dataType = "QuartzJobDTO")
    })
    public AppReturnMsg doRunJob(HttpServletRequest request, HttpServletResponse response, @PathVariable String id) {
        getBaseService().doRunJob(id);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",null);
    }

}

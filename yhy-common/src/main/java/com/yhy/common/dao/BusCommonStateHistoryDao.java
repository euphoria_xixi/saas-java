package com.yhy.common.dao;

import com.yhy.common.vo.BusCommonState;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-7 下午6:51 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "busCommonStateHistoryDao")
public interface BusCommonStateHistoryDao extends BaseDao<BusCommonState> {

    Integer deleteCommonStateById(BusCommonState commonState);

    BusCommonState findByBusModuleAndId(@Param("busModule") String busModule, @Param("id") String id);

}

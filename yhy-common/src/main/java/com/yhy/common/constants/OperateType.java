package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-8-14 上午11:10 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public enum OperateType {

	// 新建
	CREATE("CREATE","新建"),

	// 编辑
	EDIT("EDIT","编辑"),

	// 删除
	DELETE("DELETE","删除");

	private final String val;
	private final String labCode;

	private static Map<String, OperateType> operateTypeMap;

	OperateType(String val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public String getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	public static OperateType getInstByVal(String key) {
		if (operateTypeMap == null) {
			synchronized (OperateType.class) {
				if (operateTypeMap == null) {
					operateTypeMap = new HashMap<String, OperateType>();
					for (OperateType busState : OperateType.values()) {
						operateTypeMap.put(busState.getVal(), busState);
					}
				}
			}
		}
		if (!operateTypeMap.containsKey(key)) {
			throw new BusinessException("状态值" + key + "对应的OperateType枚举值不存在。");
		}
		return operateTypeMap.get(key);
	}

}

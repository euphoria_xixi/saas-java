package com.yhy.common.constants;

import com.yhy.common.exception.BusinessException;

import java.util.HashMap;
import java.util.Map;

/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */
public enum BusDirection {

	// 发送方
	SEND(1,"SEND"),

	// 接收方
	ACCEPT(2,"ACCEPT");

	private final Integer val;
	private final String labCode;

	private static Map<Integer, BusDirection> busDriectionMap;

	BusDirection(Integer val, String labCode) {
		this.val = val;
		this.labCode = labCode;
	}

	public Integer getVal() {
		return val;
	}

	public String getLabel() {
		return labCode;
	}

	public static BusDirection getInstByVal(Integer val) {
		if (busDriectionMap == null) {
			synchronized (BusDirection.class) {
				if (busDriectionMap == null) {
					busDriectionMap = new HashMap<Integer, BusDirection>();
					for (BusDirection busState : BusDirection.values()) {
						busDriectionMap.put(busState.getVal(), busState);
					}
				}
			}
		}
		String key = val + "";
		if (!busDriectionMap.containsKey(key)) {
			throw new BusinessException("状态值" + val + "对应的BusDirection枚举值不存在。");
		}
		return busDriectionMap.get(key);
	}

}

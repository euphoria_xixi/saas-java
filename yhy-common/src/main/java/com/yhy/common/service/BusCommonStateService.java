package com.yhy.common.service;

import com.yhy.common.constants.BusDirection;
import com.yhy.common.constants.BusSelfState;
import com.yhy.common.constants.BusType;
import com.yhy.common.constants.FunProcess;
import com.yhy.common.dao.BusCommonStateDao;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.BusCommonState;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;


/*
 * Copyright (c) 2019.
 * yanghuiyuan
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class BusCommonStateService extends BaseService<BusCommonState> {

    @Autowired
    private BusCommonStateHistoryService busCommonStateHistoryService;

    @Autowired
    private BusCommonStateDao busCommonStateDao;

    @Override
    protected BusCommonStateDao getDao() {
        return busCommonStateDao;
    }


    @Override
    protected void preInsert(BusCommonState entity) {
        if(StringUtils.isBlank(entity.getId())) {
            entity.setId(YhyUtils.genId(entity.getBusModule()));
        }
        super.preInsert(entity);
        if(entity.getVersion() == null) {
            entity.setVersion(1);
        }
        if (StringUtils.isBlank(entity.getEnableFlag())) {
            entity.setEnableFlag("Y");
        }
        if (StringUtils.isBlank(entity.getHistoryFlag())) {
            entity.setHistoryFlag("N");
        }
        if (entity.getBusType() == null) {
            entity.setBusType(BusType.ONE.getVal());
        }
        if (entity.getBusDate() == null) {
            entity.setBusDate(YhyUtils.getCurDateTime());
        }
        if (entity.getBusDirection() == null) {
            entity.setBusDirection(BusDirection.SEND.getVal());
        }
        if (StringUtils.isBlank(entity.getSysGenCode())) {
            entity.setSysGenCode(YhyUtils.genSysCode(entity.getBusModule()));
        }
        if (StringUtils.isBlank(entity.getRelNum())) {
            entity.setRelNum(YhyUtils.generateUUID());
        }
    }

    /*
    *  先删除正式表，然后插入到 历史表
     */
    public Integer deleteCommonStateById(String id, String busModule) {
        //2.删除业务表
        BusCommonState busCommonState = new BusCommonState();
        busCommonState.setId(id);
        busCommonState.setBusModule(busModule);
        SysUser sysUser = YhyUtils.getSysUser();
        if (sysUser != null) {
            busCommonState.setLastUpdateBy(sysUser.getUserAccount());
            busCommonState.setLastUpdateDate(YhyUtils.getCurDateTime());
        }
        return getDao().deleteCommonStateById(busCommonState);
    }

    public BusCommonState findByBusModuleAndMainId(String busModule, String mainId) {
        return getDao().findByBusModuleAndMainId(busModule, mainId);
    }

    public BusCommonState findByBusModuleAndId(String busModule, String id) {
        return getDao().findByBusModuleAndId(busModule, id);
    }

    // 更新 历史标识
    public Integer updateHistoryCommonState(String busModule, String id) {
        return getDao().updateHistoryCommonState(busModule, id);
    }

    // 恢复表的状态， 将历史表的记录移至正式表中
    public Integer updateRecoverCommonState(String busModule, String id) {
        return getDao().updateRecoverCommonState(busModule,id);
    }

    public BusCommonState updateRecoverSimpleState(String id, String busModule) {
        BusCommonState oldBusCommonState = busCommonStateHistoryService.findByBusModuleAndId(busModule, id);
        if(oldBusCommonState == null) {
            throw new BusinessException("未找到历史记录");
        }
        getDao().updateSimpleBusState(oldBusCommonState);
        return oldBusCommonState;
    }

    /*
    * 用于 简单业务(历史表中保留一条记录), 更新对应的业务状态，时间 ，功能处理类别
     */
    public Integer updateSimpleBusState(String id, String busModule, BusSelfState busSelfState,
                                        Date busdate, FunProcess funProcess, String rmk, Integer busVersion, String mainId) {
        SysUser sysUser = YhyUtils.getSysUser();
        BusCommonState busCommonState = new BusCommonState();
        busCommonState.setSelfState(busSelfState.getVal());
        busCommonState.setBusModule(busModule);
        busCommonState.setId(id);
        busCommonState.setBusDate(busdate);
        busCommonState.setFunProcess(funProcess.getVal());
        busCommonState.setRmk(rmk);
        busCommonState.setBusVersion(busVersion);
        busCommonState.setMainId(mainId);
        if (sysUser != null) {
            busCommonState.setLastUpdateBy(sysUser.getUserAccount());
            busCommonState.setLastUpdateDate(YhyUtils.getCurDateTime());
        }
        return getDao().updateSimpleBusState(busCommonState);
    }

    public Integer updateBusState(String id, String busModule, BusSelfState busSelfState) {
        SysUser sysUser = YhyUtils.getSysUser();
        BusCommonState busCommonState = new BusCommonState();
        busCommonState.setSelfState(busSelfState.getVal());
        busCommonState.setBusModule(busModule);
        busCommonState.setId(id);
        if (sysUser != null) {
            busCommonState.setLastUpdateBy(sysUser.getUserAccount());
            busCommonState.setLastUpdateDate(YhyUtils.getCurDateTime());
        }
        return getDao().updateBusState(busCommonState);
    }

    /*
    * 记录生效后， 将上一版本的记录 插入至历史表中
     */
    public Integer updateBusInvalid(String id, String busModule) {
        BusCommonState invalidCommonState = this.findByBusModuleAndId(busModule,id);
        SysUser sysUser = YhyUtils.getSysUser();
        invalidCommonState.setSelfState(BusSelfState.INVALID.getVal());
        invalidCommonState.setVersion(invalidCommonState.getVersion()+1);
        if (sysUser != null) {
            invalidCommonState.setLastUpdateBy(sysUser.getUserAccount());
            invalidCommonState.setLastUpdateDate(YhyUtils.getCurDateTime());
        }
        busCommonStateDao.deleteById(invalidCommonState);
        return busCommonStateHistoryService.insert(invalidCommonState);
    }

    /*
     * 记录生效后， 将上一版本的记录 插入至历史表中, 用于简单业务
     */
    public void saveSimpleHistory(BusCommonState oldCommonState) {
        BusCommonState existBusCommonState = busCommonStateHistoryService.findByBusModuleAndId(oldCommonState.getBusModule(), oldCommonState.getId());
        if(existBusCommonState == null) {
            busCommonStateHistoryService.insert(oldCommonState);
        } else {
            existBusCommonState.setId(null);
            existBusCommonState.setHistoryFlag("Y");
            busCommonStateHistoryService.insert(existBusCommonState);
            busCommonStateHistoryService.update(oldCommonState);
        }
    }

}

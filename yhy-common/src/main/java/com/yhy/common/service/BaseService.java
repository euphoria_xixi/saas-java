package com.yhy.common.service;

import com.github.pagehelper.PageHelper;
import com.google.common.collect.Lists;
import com.yhy.common.dao.BaseDao;
import com.yhy.common.dto.BaseEntity;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.PageVO;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public abstract class BaseService<T extends BaseEntity> {

    protected final Logger LOGGER = LoggerFactory.getLogger(getClass());

    @Autowired
    private DataSource dataSource;

    /**
     * 获取jdbc模版
     *
     * @return
     */
    protected JdbcTemplate getJdbcTemplate() {
        return new JdbcTemplate(dataSource);
    }

    protected BaseDao<T> getDao() {
        return null;
    }

    public List<T> findAll() {
        return getDao().findAll();
    }

    public T findUniqueBy(T entity) {
        List<T> rtnlist = getDao().findBy(entity);
        if(rtnlist == null || rtnlist.isEmpty()) {
            return null;
        }
        return rtnlist.get(0);
    }

    public List<T> findBy(T entity) {
        return getDao().findBy(entity);
    }

    public Boolean hasBy(T entity) {
        List<T> rtnList = getDao().findBy(entity);
        if(rtnList == null || rtnList.isEmpty()) {
            return false;
        }
        return true;
    }

    public T findById(String id) {
        return getDao().findById(id);
    }

    public void saveOrUpdate(T entity) {
        if (StringUtils.isBlank(entity.getId())) {
            insert(entity);
        } else {
            update(entity);
        }
    }

    protected void preUpdate(T entity) {
        SysUser sysUser = YhyUtils.getSysUser();
        if(sysUser != null) {
            entity.setLastUpdateBy(sysUser.getUserAccount());
        }
        entity.setLastUpdateDate(YhyUtils.getCurDateTime());
    }

    public int deleteBy(T entity) {
        return getDao().deleteBy(entity);
    }

    public int deleteById(T entity) {
        preUpdate(entity);
        return getDao().deleteById(entity);
    }

    public int update(T entity) {
        preUpdate(entity);
        return getDao().update(entity);
    }

    public int updateNotEmpty(T entity) {
        preUpdate(entity);
        return getDao().updateNotEmpty(entity);
    }

    protected void preInsert(T entity) {
        entity.setEnableFlag("Y");
        if(StringUtils.isBlank(entity.getId())) {
            entity.setId(YhyUtils.genId(""));
        }
        SysUser sysUser = YhyUtils.getSysUser();
        if(sysUser != null) {
            if(StringUtils.isBlank(entity.getCreateBy())) {
                entity.setCreateBy(sysUser.getUserAccount());
            }
            if(StringUtils.isBlank(entity.getLastUpdateBy())) {
                entity.setLastUpdateBy(sysUser.getUserAccount());
            }
            if(StringUtils.isBlank(entity.getSysOwnerCpy())) {
                entity.setSysOwnerCpy(sysUser.getCpyCode());
            }
            if(StringUtils.isBlank(entity.getSysOrgCode())) {
                entity.setSysOrgCode(sysUser.getOrgCode());
            }
        }
        if(entity.getCreateDate() == null) {
            entity.setCreateDate(YhyUtils.getCurDateTime());
        }
        if(entity.getLastUpdateDate() == null) {
            entity.setLastUpdateDate(YhyUtils.getCurDateTime());
        }
    }

    public int insert(T entity) {
        preInsert(entity);
        return getDao().insert(entity);
    }

    public void batchInsert(List<T> entities) {
        for (T entity : entities) {
            preInsert(entity);
        }
        getDao().batchInsert(entities);
    }

    public void batchUpdate(List<T> entities) {
        getDao().batchUpdate(entities);
    }


    public List findDataByPage(T entity, PageVO pageBean) {
        if (pageBean == null) {
            return getDao().queryPage(entity);
        }
        Long totalCount = getDao().queryPageCount(entity);
        if (totalCount == 0) {
            return Lists.newArrayList();
        }
        pageBean.setTotalCount(totalCount);
        PageHelper.startPage(pageBean.getPageNo(), pageBean.getPageSize(), false);
        return getDao().queryPage(entity);
    }

}

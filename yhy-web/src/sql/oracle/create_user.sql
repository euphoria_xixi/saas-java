--应用访问用户
create user yhy_app identified by yhy_app;
-- Grant/Revoke role privileges
grant connect to yhy_app;
grant resource to yhy_app;
-- Grant/Revoke system privileges
grant alter any procedure to yhy_app;
grant alter any table to yhy_app;
grant create any index to yhy_app;
grant create any procedure to yhy_app;
grant create any table to yhy_app;
grant create any view to yhy_app;
grant create session to yhy_app;
grant delete any table to yhy_app;
grant drop any index to yhy_app;
grant drop any procedure to yhy_app;
grant drop any table to yhy_app;
grant drop any view to yhy_app;
grant insert any table to yhy_app;
grant select any table to yhy_app;
grant unlimited tablespace to yhy_app;
grant update any table to yhy_app;


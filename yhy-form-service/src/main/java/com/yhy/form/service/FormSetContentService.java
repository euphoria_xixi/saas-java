package com.yhy.form.service;

import com.yhy.common.service.BaseService;
import com.yhy.form.dao.FormSetContentDao;
import com.yhy.form.vo.FormSetContentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-1-10 上午11:45 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */

@Service
@Transactional(rollbackFor = Exception.class)
public class FormSetContentService extends BaseService<FormSetContentVO> {


    @Autowired
    private FormSetContentDao baseDao;

    @Override
    protected FormSetContentDao getDao() {
        return baseDao;
    }

    public FormSetContentVO findByMainId(String mainId) {
        return getDao().findByMainId(mainId);
    }

}

package com.yhy.form.dao;

import com.yhy.common.dao.BaseComplexMngDao;
import com.yhy.common.dao.BaseMngDao;
import com.yhy.common.dao.BaseSimpleMngDao;
import com.yhy.form.dto.FormApplyDTO;
import com.yhy.form.dto.FormSetDTO;
import com.yhy.form.vo.mng.FormApplyMngVO;
import com.yhy.form.vo.mng.FormSetMngVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-4-17 上午11:03 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@Mapper
@Component(value = "formApplyMngDao")
public interface FormApplyMngDao extends BaseSimpleMngDao<FormApplyDTO, FormApplyMngVO> {

    Boolean checkExistProcessRecord(@Param("formCode")String formCode);

}

package com.yhy.admin.vo.mng;

import com.yhy.admin.vo.SysRoleFormVO;
import com.yhy.admin.vo.SysRoleMenuVO;
import com.yhy.admin.vo.SysRoleOrgVO;
import com.yhy.admin.vo.SysRoleUserVO;
import com.yhy.common.dto.BaseMngVO;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:27 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class SysRoleMngVO extends BaseMngVO {

    private String roleName;

    private List<SysRoleMenuVO> roleMenuVOList;
    private List<SysRoleUserVO>  roleUserVOList;
    private List<SysRoleOrgVO>   roleOrgVOList;
    private List<SysRoleFormVO>   roleFormVOList;

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }


    public List<SysRoleMenuVO> getRoleMenuVOList() {
        return roleMenuVOList;
    }

    public void setRoleMenuVOList(List<SysRoleMenuVO> roleMenuVOList) {
        this.roleMenuVOList = roleMenuVOList;
    }

    public List<SysRoleUserVO> getRoleUserVOList() {
        return roleUserVOList;
    }

    public void setRoleUserVOList(List<SysRoleUserVO> roleUserVOList) {
        this.roleUserVOList = roleUserVOList;
    }

    public List<SysRoleOrgVO> getRoleOrgVOList() {
        return roleOrgVOList;
    }

    public void setRoleOrgVOList(List<SysRoleOrgVO> roleOrgVOList) {
        this.roleOrgVOList = roleOrgVOList;
    }

    public List<SysRoleFormVO> getRoleFormVOList() {
        return roleFormVOList;
    }

    public void setRoleFormVOList(List<SysRoleFormVO> roleFormVOList) {
        this.roleFormVOList = roleFormVOList;
    }
}

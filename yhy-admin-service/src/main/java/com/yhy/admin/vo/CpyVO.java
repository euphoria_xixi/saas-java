package com.yhy.admin.vo;

import com.yhy.common.dto.BaseEntity;
import com.yhy.common.dto.BaseMainEntity;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-6-28 上午9:47 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
public class CpyVO extends BaseMainEntity {

    private String cpyName;
    private String cpyLongName;
    private String cpyType;
    private String cpyCode;
    private String cerCode;
    private String ownerApp;
    private String cpyStatus;


    public CpyVO() {
    }

    public String getCpyName() {
        return cpyName;
    }

    public void setCpyName(String cpyName) {
        this.cpyName = cpyName;
    }

    public String getCpyLongName() {
        return cpyLongName;
    }

    public void setCpyLongName(String cpyLongName) {
        this.cpyLongName = cpyLongName;
    }

    public String getCpyType() {
        return cpyType;
    }

    public void setCpyType(String cpyType) {
        this.cpyType = cpyType;
    }

    public String getCpyCode() {
        return cpyCode;
    }

    public void setCpyCode(String cpyCode) {
        this.cpyCode = cpyCode;
    }

    public String getCerCode() {
        return cerCode;
    }

    public void setCerCode(String cerCode) {
        this.cerCode = cerCode;
    }

    public String getOwnerApp() {
        return ownerApp;
    }

    public void setOwnerApp(String ownerApp) {
        this.ownerApp = ownerApp;
    }

    public String getCpyStatus() {
        return cpyStatus;
    }

    public void setCpyStatus(String cpyStatus) {
        this.cpyStatus = cpyStatus;
    }
}

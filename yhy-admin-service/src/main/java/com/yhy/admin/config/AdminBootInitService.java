package com.yhy.admin.config;

import com.yhy.common.exception.CustomRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(value = 3)
public class AdminBootInitService implements CommandLineRunner {
    private static final Logger LOGGER = LoggerFactory.getLogger(AdminBootInitService.class);

    @Override
    public void run(String... strings) throws Exception {
        try {
            //加载系统配置信息
            /*SysInitializationService sysRegService = SpringContextHolder.getBean(SysInitializationService.class);
            List<SysInitialization> sysInitializations = sysRegService.findBy(null);
            if (!sysInitializations.isEmpty()) {
                SysInfo sysInfo = transSysInfo(sysInitializations);
                AdminHelper.setSysInfo(sysInfo);
                //RedisUtil.set(AdminConstants.ADMIN_APP_ENV_INIT_SYS_INFO, sysInfo);
            } else {
                throw new BusinessException("系统初始化信息丢失。");
            }*/
        } catch (Exception ex) {
            LOGGER.error("admin app init fail", ex);
            throw new CustomRuntimeException("admin app init fail", ex);
        }

        //BusDaynamicDSService daynamicDSService = SpringContextHolder.getBean(BusDaynamicDSService.class);
        //加载多数据源
        //daynamicDSService.initDataSource();
    }

}

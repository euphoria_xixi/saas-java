package com.yhy.admin.service;

import com.yhy.admin.dao.SysUserDao;
import com.yhy.admin.dao.SysUserMngDao;
import com.yhy.admin.dto.LoginDTO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.constants.BusPrivCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.service.BaseService;
import com.yhy.common.token.JwtHelper;
import com.yhy.common.utils.ConstantUtil;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.RedisUtil;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@Service
@Transactional(rollbackFor = Exception.class)
public class LoginService extends BaseService {

    @Autowired
    private SysUserMngDao sysUserMngDao;
    @Autowired
    private SysUserDao sysUserDao;

    public void changePassword(LoginDTO loginDTO) {
        if(!loginDTO.getPassword().equals(loginDTO.getConfirmPassword())) {
            throw new BusinessException("二者密码不一致");
        }
        SysUserMngVO sysUser = sysUserMngDao.findByUserAccount(loginDTO.getUserAccount());
        //String oldPasswd = YhyUtils.toMd5(loginDTO.getOldPassword()+loginDTO.getUserAccount());
        if(!loginDTO.getOldPassword().equals(sysUser.getPassword())) {
            throw new BusinessException("旧密码不对");
        }
        //String encodePasswd = YhyUtils.toMd5(loginDTO.getPassword()+loginDTO.getUserAccount());
        //loginDTO.setPassword(encodePasswd);
        sysUserDao.updatePassword(loginDTO.getUserAccount(),loginDTO.getPassword());
    }

    public SysUser checkUserAndPassword(LoginDTO loginDTO) {
        //String encodePasswd = YhyUtils.toMd5(loginDTO.getPassword() + loginDTO.getUserAccount());
        //loginDTO.setPassword(encodePasswd);
        SysUserMngVO sysUser = sysUserMngDao.checkUserAndPassword(loginDTO.getUserAccount(),loginDTO.getPassword());
        return JsonUtils.tranObject(sysUser,SysUser.class);
    }

    public String tokenOfLoginSucess(LoginDTO loginDTO,SysUser sysUser) {
		Map<String , Object> payload=new HashMap<String, Object>();
		Date date=new Date();
		payload.put(JwtHelper.CURRENT_USER, loginDTO.getUserAccount());//用户id
		payload.put(JwtHelper.SOURCE_TYPE, loginDTO.getSourceType());//来源
		//payload.put("ext",date.getTime()+JwtHelper.TOKEN_EXPIRES_MILS);//过期时间48小时
		payload.put("iat", date.getTime());//生成时间
        sysUser.setSourceType(loginDTO.getSourceType());
        if(sysUser.isSysAdmin() && StringUtils.isBlank(sysUser.getCpyCode())) {
            sysUser.setCpyCode(BusPrivCode.PRIV_SYS_ADMIN.getVal());
        }
        payload.put(JwtHelper.SYS_USER,JsonUtils.toJson(sysUser));

		String newToken = JwtHelper.createToken(payload);

		//先清空缓存
		logout(loginDTO);

		JwtHelper.putToken(ConstantUtil.TOKEN_PREFIX +loginDTO.getSourceType() + loginDTO.getUserAccount(), newToken,JwtHelper.TOKEN_EXPIRES_SECONDS);
		RedisUtil.set(ConstantUtil.OPERATOR_PREFIX + loginDTO.getSourceType() + loginDTO.getUserAccount(), sysUser,JwtHelper.TOKEN_EXPIRES_SECONDS);
		return newToken;
    }

    public void logout(LoginDTO loginDTO) {
        RedisUtil.remove(ConstantUtil.TOKEN_PREFIX + loginDTO.getSourceType() + loginDTO.getUserAccount());
        RedisUtil.remove(ConstantUtil.OPERATOR_PREFIX + loginDTO.getSourceType() + loginDTO.getUserAccount());
        RedisUtil.remove(ConstantUtil.PRIV_CODE_PREFIX + loginDTO.getSourceType() + loginDTO.getUserAccount());
        RedisUtil.remove(ConstantUtil.MENU_PREFIX +loginDTO.getSourceType() + loginDTO.getUserAccount());
    }

}

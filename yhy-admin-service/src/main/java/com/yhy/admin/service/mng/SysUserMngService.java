package com.yhy.admin.service.mng;

import com.yhy.admin.dao.SysUserMngDao;
import com.yhy.admin.dto.SysUserDTO;
import com.yhy.admin.service.SysUserJobService;
import com.yhy.admin.service.SysUserService;
import com.yhy.admin.service.UserCpyService;
import com.yhy.admin.service.UserMgrService;
import com.yhy.admin.vo.CpyVO;
import com.yhy.admin.vo.SysUserJobVO;
import com.yhy.admin.vo.UserMgrVO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.admin.vo.UserCpyVO;
import com.yhy.common.constants.BusModuleType;
import com.yhy.common.service.BaseMngService;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b><br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 19-11-13 下午4:26 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2019<br>
 *  *
 *
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysUserMngService extends BaseMngService<SysUserDTO,SysUserMngVO> {

    @Autowired
    private SysUserMngDao sysUserMngDao;

    @Autowired
    private SysUserService sysUserService;
    @Autowired
    private UserCpyService userCpyService;
    @Autowired
    private UserMgrService userMgrService;
    @Autowired
    private SysUserJobService userJobService;


    @Override
    public SysUserService getBaseMainService() {
        return sysUserService;
    }

    @Override
    protected SysUserMngDao getBaseMngDao() {
        return sysUserMngDao;
    }

    @Override
    public String getBusModule() {
        return BusModuleType.MD_USER_MNG.getVal();
    }


    @Override
    protected void processOfToAdd(SysUserDTO baseDTO) {
        super.processOfToAdd(baseDTO);
        SysUserMngVO mngVO = baseDTO.getBusMainData();
        mngVO.setUserStatus("UNLOCK");
        mngVO.setEnableFlag("Y");
    }

    @Override
    protected void beforeSaveOfProcess(SysUserDTO baseDTO) {
        super.beforeSaveOfProcess(baseDTO);
        SysUserMngVO sysUserMngVO = baseDTO.getBusMainData();
        /*if(StringUtils.isBlank(sysUserMngVO.getId())) {
            sysUserMngVO.setPassword(YhyUtils.toMd5(sysUserMngVO.getPassword()+sysUserMngVO.getUserAccount()));
        }*/
        sysUserMngVO.setUserAccount(StringUtils.lowerCase(sysUserMngVO.getUserAccount()));
        sysUserMngVO.setCpyCode(null);
        if(sysUserMngVO.getCpyVOS() != null &&  !sysUserMngVO.getCpyVOS().isEmpty()) {
            //设定一个主公司编号
            for (CpyVO cpyVO : sysUserMngVO.getCpyVOS()) {
                sysUserMngVO.setCpyCode(cpyVO.getCpyCode());
                break;
            }
        }
    }

    @Override
    protected void afterProcessCustomDataOfSave(SysUserDTO baseDTO) {
        super.afterProcessCustomDataOfSave(baseDTO);
        SysUserMngVO sysUserMngVO = baseDTO.getBusMainData();
        if(sysUserMngVO.getCpyVOS() != null) {
            userCpyService.deleteByUserAccount(sysUserMngVO.getUserAccount());
            for (CpyVO cpyVO : sysUserMngVO.getCpyVOS()) {
                UserCpyVO userCpyVO = new UserCpyVO();
                userCpyVO.setUserId(sysUserMngVO.getId());
                userCpyVO.setUserAccount(sysUserMngVO.getUserAccount());
                userCpyVO.setCpyCode(cpyVO.getCpyCode());
                userCpyVO.setEnableFlag("Y");
                userCpyService.insert(userCpyVO);
            }
        }
    }

    @Override
    protected void afterOfProcessLoadData(SysUserDTO orgSetDTO) {
        super.afterOfProcessLoadData(orgSetDTO);

        List<SysUserMngVO> rtnList = orgSetDTO.getRtnList();
        for (SysUserMngVO sysUserMngVO : rtnList) {
            List<UserCpyVO> userCpyVOS = userCpyService.findUserCpyByUserAccount(sysUserMngVO.getUserAccount());
            sysUserMngVO.setCpyVOS(JsonUtils.tranList(userCpyVOS,CpyVO.class));
            for (UserCpyVO cpyVO : userCpyVOS) {
                if(cpyVO.getCpyCode().equals(sysUserMngVO.getCpyCode())) {
                    sysUserMngVO.setCpyName(cpyVO.getCpyName());
                    break;
                }
            }
        }
    }

    public List<SysUserMngVO> findUserByOrgCode(String cpyCode,String orgCode) {
        List<SysUserMngVO> userMngVOS =  sysUserMngDao.findUserByOrgCode(cpyCode,orgCode);
        for (SysUserMngVO sysUserMngVO : userMngVOS) {
            UserMgrVO userMgrVO = userMgrService.findMgrByUserAccount(cpyCode,sysUserMngVO.getUserAccount());
            if(userMgrVO != null)
                sysUserMngVO.setMgrAccount(userMgrVO.getMgrAccount());
            SysUserJobVO userJobVO = userJobService.findJobByUserId(cpyCode,sysUserMngVO.getId());
            if(userJobVO != null) {
                sysUserMngVO.setJobId(userJobVO.getJobId());
                sysUserMngVO.setJobName(userJobVO.getJobName());
            }
        }
        return userMngVOS;
    }

    public List<SysUserMngVO> findUserByCpyCode(String cpyCode) {
        List<SysUserMngVO> userMngVOS =  sysUserMngDao.findUserByCpyCode(cpyCode);
        for (SysUserMngVO sysUserMngVO : userMngVOS) {
            UserMgrVO userMgrVO = userMgrService.findMgrByUserAccount(cpyCode,sysUserMngVO.getUserAccount());
            if(userMgrVO != null)
                sysUserMngVO.setMgrAccount(userMgrVO.getMgrAccount());
            SysUserJobVO userJobVO = userJobService.findJobByUserId(cpyCode,sysUserMngVO.getId());
            if(userJobVO != null) {
                sysUserMngVO.setJobId(userJobVO.getJobId());
                sysUserMngVO.setJobName(userJobVO.getJobName());
            }
        }
        return userMngVOS;
    }

    public void updateAvatar(String userAccount,String avatar) {
        sysUserService.updateAvatar(userAccount,avatar);
    }

    public void updateCpyOrgSet(String userAccount,String cpyCode,String orgCode) {
        sysUserService.updateCpyOrgSet(userAccount, cpyCode, orgCode);
    }

    public void updateOrgSet(String userAccount,String orgCode) {
        sysUserService.updateOrgSet(userAccount, orgCode);
    }

    public SysUser findByPhone(String phone) {
        SysUserMngVO sysUser = sysUserMngDao.findByPhone(phone);
        return JsonUtils.tranObject(sysUser,SysUser.class);
    }

    public SysUser findByUserCpyCode(String userAccount, String cpyCode) {
        SysUserMngVO sysUser = sysUserMngDao.findByUserCpyCode(userAccount, cpyCode);
        return JsonUtils.tranObject(sysUser,SysUser.class);
    }

    /*
    * 只适用于当前用户所操作的公司
     */
    @Deprecated
    public SysUser findByUserAccount(String userAccount) {
        SysUserMngVO sysUser = sysUserMngDao.findByUserAccount(userAccount);
        return JsonUtils.tranObject(sysUser,SysUser.class);
    }

    public void updateUserInfo(SysUserMngVO sysUserMngVO) {
        SysUser sysUser = JsonUtils.tranObject(sysUserMngVO,SysUser.class);
        sysUserService.update(sysUser);

        //保存上级信息
        UserMgrVO userMgrVO = new UserMgrVO();
        userMgrVO.setCpyCode(sysUserMngVO.getCpyCode());
        userMgrVO.setMgrAccount(sysUserMngVO.getMgrAccount());
        userMgrVO.setUserAccount(sysUserMngVO.getUserAccount());
        userMgrVO.setUserId(sysUserMngVO.getId());
        userMgrService.saveUserMgr(userMgrVO);

        //保存职务信息
        SysUserJobVO userJobVO = new SysUserJobVO();
        userJobVO.setCpyCode(sysUserMngVO.getCpyCode());
        userJobVO.setJobName(sysUserMngVO.getJobName());
        userJobVO.setJobId(sysUserMngVO.getJobId());
        userJobVO.setUserId(sysUserMngVO.getId());
        userJobService.saveUserJob(userJobVO);
    }


}

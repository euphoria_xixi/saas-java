package com.yhy.admin.action;

import com.yhy.admin.dto.OrgSetDTO;
import com.yhy.admin.service.UserMgrService;
import com.yhy.admin.service.mng.OrgSetMngService;
import com.yhy.admin.vo.UserMgrVO;
import com.yhy.admin.vo.mng.OrgSetMngVO;
import com.yhy.admin.vo.mng.SysUserMngVO;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.JsonUtils;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

/*
 * 组织机构设置
 * yanghuiyuan 2019/6/17
 */
@RestController
@RequestMapping(value="/admin-service/api/orgSetMng", produces="application/json;charset=UTF-8")
public class OrgSetMngAction extends BaseMngAction<OrgSetDTO> {

    @Override
    protected OrgSetMngService getBaseService() {
        return SpringContextHolder.getBean(OrgSetMngService.class);
    }

    /*
     * 根据用户公司编号获取组织
     */
    @RequestMapping(value="/getOrgSetsByUserCpyCode",method = RequestMethod.GET)
    @ResponseBody
    public AppReturnMsg getOrgSetsByUserCpyCode(HttpServletRequest request, HttpServletResponse response) {
        SysUser sysUser = YhyUtils.getSysUser();
        List<OrgSetMngVO> orgSetMngVOList = getBaseService().findOrgSetByUserCpyCode(sysUser.getUserAccount(),sysUser.getCpyCode());
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", orgSetMngVOList,null);
    }

    /*
     * 根据公司编号获取组织
     */
    @RequestMapping(value="/getOrgSetsByCpyCode",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="根据公司编号获取组织", notes="根据公司编号获取组织")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyCode", value = "公司编码", required = true, dataType = "String")
    })
    public AppReturnMsg getOrgSetsByCpyCode(HttpServletRequest request, HttpServletResponse response,@RequestBody OrgSetMngVO orgSetMngVO) {
        OrgSetDTO baseDTO =new OrgSetDTO();

        orgSetMngVO.setEnableFlag("Y");
        baseDTO.setParamBean(orgSetMngVO);

        List<OrgSetMngVO> mngVOS =   getBaseService().queryPageData(baseDTO);
        Set<OrgSetMngVO> tree = getBaseService().buildOrgTree(mngVOS);

        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"", JsonUtils.tranList(tree, HashMap.class),null);
    }

    @Override
    protected void afterLoadDataInfo(OrgSetDTO baseDTO,AppReturnMsg returnMsg ) {
        Set<OrgSetMngVO> tree = getBaseService().buildOrgTree(baseDTO.getRtnList());
        returnMsg.setData(tree);
    }

    @Override
    protected void beforeOfSaveProcess(OrgSetDTO baseDTO) {
        super.beforeOfSaveProcess(baseDTO);
        OrgSetMngVO mngVO = baseDTO.getBusMainData();
        if(StringUtils.isBlank(mngVO.getCpyCode())) {
            mngVO.setCpyCode(YhyUtils.getSysUser().getCpyCode());
        }
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ORGSET_ALL","ORGSET_CREATE","ORGSET_CHANGE"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = OrgSetDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody OrgSetDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ORGSET_ALL","ORGSET_CREATE","ORGSET_CHANGE"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = OrgSetDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody OrgSetDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ORGSET_ALL","ORGSET_CREATE","ORGSET_CHANGE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "OrgSetDTO")
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody OrgSetDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ORGSET_ALL","ORGSET_SELECT"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "OrgSetDTO")
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody OrgSetDTO baseDTO) {
        SysUser sysUser = YhyUtils.getSysUser();
        baseDTO.getParamBean().setCpyCode(sysUser.getCpyCode());
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ORGSET_ALL","ORGSET_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataType = "OrgSetDTO")
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody OrgSetDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }


    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ORGSET_ALL","ORGSET_CREATE","ORGSET_CHANGE"})
    @RequestMapping(value="/saveUserOrgSet",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存用户与部门关系", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataType = "OrgSetDTO")
    })
    public AppReturnMsg saveUserOrgSet(HttpServletRequest request, HttpServletResponse response, @RequestBody OrgSetDTO baseDTO) {
        return getBaseService().saveUserOrgSet(baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","ORGSET_ALL","ORGSET_DELETE"})
    @RequestMapping(value="/deleteUserOrgSet",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除部门与用户关系", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataTypeClass = OrgSetDTO.class)
    })
    public AppReturnMsg deleteUserOrgSet(HttpServletRequest request, HttpServletResponse response, @RequestBody OrgSetDTO baseDTO) {
        return getBaseService().deleteUserOrgSet(baseDTO);
    }

}

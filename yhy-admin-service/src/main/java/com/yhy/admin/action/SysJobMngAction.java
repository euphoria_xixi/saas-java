package com.yhy.admin.action;

import com.yhy.admin.dto.SysJobDTO;
import com.yhy.admin.service.mng.SysJobMngService;
import com.yhy.admin.vo.mng.SysJobMngVO;
import com.yhy.common.action.BaseMngAction;
import com.yhy.common.dto.AppReturnMsg;
import com.yhy.common.dto.ReturnCode;
import com.yhy.common.exception.BusinessException;
import com.yhy.common.intercept.PreCheckPermission;
import com.yhy.common.utils.SpringContextHolder;
import com.yhy.common.utils.YhyUtils;
import com.yhy.common.vo.SysUser;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/*
 *
 *  * *
 *  *  * <br>
 *  *  * <b>功能：</b>职务设定<br>
 *  *  * <b>作者：</b>yanghuiyaun<br>
 *  *  * <b>日期：</b> 20-3-25 下午2:51 <br>
 *  *  * <b>版权所有：<b>版权所有(C) 2020<br>
 *  *
 *
 */
@RestController
@RequestMapping(value="/admin-service/api/sysJobMng", produces="application/json;charset=UTF-8")
public class SysJobMngAction extends BaseMngAction<SysJobDTO> {

    @Override
    protected SysJobMngService getBaseService() {
        return SpringContextHolder.getBean(SysJobMngService.class);
    }
    /*
     * 获取职务
     */
    @RequestMapping(value="/findJobByCpyCode",method = RequestMethod.POST)
    @ResponseBody
    @ApiOperation(value="根据公司编号获取职务", notes="根据公司编号获取职务")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpyCode", value = "公司编码", required = true, dataTypeClass =SysJobDTO.class)
    })
    public AppReturnMsg findJobByCpyCode(HttpServletRequest request, HttpServletResponse response,@RequestBody SysJobDTO baseDTO) {
        if(StringUtils.isBlank(baseDTO.getParamBean().getCpyCode())) {
            throw new BusinessException("公司编号不能为空");
        }
        getBaseService().loadData(baseDTO);
        return new AppReturnMsg(ReturnCode.SUCCESS_CODE.getCode(),"",baseDTO,null);
    }

    @Override
    protected void beforeOfSaveProcess(SysJobDTO baseDTO) {
        super.beforeOfSaveProcess(baseDTO);
        SysJobMngVO mngVO = baseDTO.getBusMainData();
        if(StringUtils.isBlank(mngVO.getCpyCode())) {
            mngVO.setCpyCode(YhyUtils.getSysUser().getCpyCode());
        }
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSJOB_ALL","SYSJOB_CREATE","SYSJOB_CHANGE"})
    @RequestMapping(value="/toAdd",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="新增操作", notes="新增操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysJobDTO.class)
    })
    public AppReturnMsg toAdd(HttpServletRequest request, HttpServletResponse response, @RequestBody SysJobDTO baseDTO) {
        return super.toAddCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSJOB_ALL","SYSJOB_CREATE","SYSJOB_CHANGE"})
    @RequestMapping(value="/toCopy",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="复制操作", notes="复制操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "提交对象ID", required = true, dataTypeClass = SysJobDTO.class)
    })
    public AppReturnMsg toCopy(HttpServletRequest request, HttpServletResponse response, @RequestBody SysJobDTO baseDTO) {
        return super.toCopyCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSJOB_ALL","SYSJOB_CREATE","SYSJOB_CHANGE"})
    @RequestMapping(value="/doSave",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="保存", notes="保存操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass =SysJobDTO.class)
    })
    public AppReturnMsg doSave(HttpServletRequest request, HttpServletResponse response, @RequestBody SysJobDTO baseDTO) {
        return super.doSaveCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSJOB_ALL","SYSJOB_SELECT"})
    @RequestMapping(value="/loadData",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="查询数据", notes="查询操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "数据传输对象", required = true, dataTypeClass =SysJobDTO.class)
    })
    public AppReturnMsg loadData(HttpServletRequest request, HttpServletResponse response, @RequestBody SysJobDTO baseDTO) {
        SysUser sysUser = YhyUtils.getSysUser();
        baseDTO.getParamBean().setCpyCode(sysUser.getCpyCode());
        return super.loadDataCommon(request, response, baseDTO);
    }

    @PreCheckPermission(roles = {"SYS_ADMIN","COMMON_ADMIN","SYSJOB_ALL","SYSJOB_DELETE"})
    @RequestMapping(value="/doDelete",method = {RequestMethod.POST})
    @ResponseBody
    @ApiOperation(value="删除", notes="删除操作")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "baseDTO", value = "删除", required = true, dataTypeClass =SysJobDTO.class)
    })
    public AppReturnMsg doDelete(HttpServletRequest request, HttpServletResponse response, @RequestBody SysJobDTO baseDTO) {
        return super.doDeleteCommon(request, response, baseDTO);
    }


}
